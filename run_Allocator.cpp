// ----------------
// RunAllocator.cpp
// ----------------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>
#include <string>
#include <cstdint>

#include "Allocator.hpp"

using namespace std;

template <typename T, std::size_t N>
void print_allocator(My_Allocator<T, N>& allocator) {
    vector<int> vec;
    allocator.for_each_block([&vec] (int size) {
        vec.push_back(size);
    });
    for(size_t i = 0; i < vec.size(); i++) {
        std::cout << vec[i];
        if(i != vec.size() - 1)
            std::cout << ' ';
    }
    cout << endl;
}

int main () {
    using namespace std;

    //read input:
    int n;
    string line;

    cin >> n;
    cin.ignore(); // Ignore the newline character after reading 'n'
    cin.ignore(); // Ignore the blank line above the first group.

    // Read the n groups
    vector<vector<int>> tcs;
    for (int i = 0; i < n; ++i) {
        vector<int> tc;
        while (true) {
            getline(cin, line);
            if(line.empty()) {
                break;
            }
            else {
                istringstream iss(line);
                int num;
                iss >> num;
                tc.push_back(num);
            }
            if(cin.eof())
                break;
        }
        tcs.push_back(tc);
    }

    for(const vector<int>& tc : tcs) {
        My_Allocator<double, 1000> a;
        for(int value : tc) {
            if(value > 0) {
                a.allocate(value);
            }
            else if(value < 0) {
                My_Allocator<double, 1000>::iterator itr = begin(a);
                while(*itr > 0)
                    ++itr;
                while(value < -1) {
                    while(*itr > 0)
                        ++itr;
                    ++itr;
                    value++;
                }
                int& size = *itr;
                double * ptr = (double*) (((intptr_t) &size) + 4);
                a.deallocate(ptr, size);
            }
        }
        print_allocator(a);
    }

    return 0;

}