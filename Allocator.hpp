// -------------
// Allocator.hpp
// -------------

#ifndef Allocator_hpp
#define Allocator_hpp

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <string>

template <typename T, std::size_t N>
class My_Allocator {

    friend bool operator==(const My_Allocator &, const My_Allocator &) {
        return false;
    }

    friend bool operator!=(const My_Allocator &lhs, const My_Allocator &rhs) {
        return !(lhs == rhs);
    }

public:
    class iterator {

        friend bool operator==(const iterator &lhs, const iterator &rhs) {
            return *lhs == *rhs;
        }

        friend bool operator!=(const iterator &lhs, const iterator &rhs) {
            return !(lhs == rhs);
        }

    private:
        My_Allocator& allocator ;
        std::size_t i;

        int &get_int(std::size_t index_in_bytes) const {
            return *((int*) (allocator.a + index_in_bytes));
        }

    public:
        /** i is the byte index. 0 <= i <= N */
        iterator(My_Allocator& r, std::size_t i) : allocator(r), i(i) {
        }

        /**
         * beginning sentinel of the block
         * Returns this sentinel - i.e. the number of allocated bytes.
         */
        int &operator*() const {
            return get_int(i);
        }

        //pre-increment
        iterator &operator++() {
            int size = (*this).operator*();
            i += 4 + (size < 0 ? -size : size) + 4;
            return *this;
        }

        iterator operator++(int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        iterator &operator--() {
            int size = get_int(i - 4);
            i -= (4 + (size < 0 ? -size : size) + 4);
            return *this;
        }

        iterator operator--(int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    class const_iterator {

        friend bool operator==(const const_iterator &lhs, const const_iterator &rhs) {
            return *lhs == *rhs;
        }

        friend bool operator!=(const const_iterator &lhs, const const_iterator &rhs) {
            return !(lhs == rhs);
        }

    private:
        const My_Allocator& allocator ;
        std::size_t i;

        const int &get_int(std::size_t index_in_bytes) const {
            return *((int*) (allocator.a + index_in_bytes));
        }

    public:

        const_iterator(const My_Allocator &r, std::size_t i) : allocator(r), i(i) {

        }

        // beginning sentinel of the block
        const int &operator*() const {
            return get_int(i);
        }

        const_iterator &operator++() {
            const int size = (*this).operator*();
            i += 4 + (size < 0 ? -size : size) + 4;
            return *this;
        }

        const_iterator operator++(int) {
            const_iterator tmp = *this;
            ++*this;
            return tmp;
        }

        const_iterator &operator--() {
            const int size = get_int(i - 4);
            i -= 4 + (size < 0 ? -size : size) + 4;
            return *this;
        }

        const_iterator operator--(int) {
            const_iterator tmp = *this;
            --*this;
            return tmp;
        }
    };

private:
    char a[N]; // array of bytes

    /**
     * Does some breif validation stuff using assert statements.
     * O(1) in space, time
     */
    bool valid() const {
        //Since this validation is meant to be O(1) in space and time,
        //there's not much we test here. Just test if the first and last
        //blocks look OK using the iterators.
        const_iterator first = begin();
        int first_start_size = *first;
        int first_end_size = *((int*) (a + (first_start_size < 0 ? -first_start_size : first_start_size) + sizeof(int)));
        const_iterator last = end();
        --last;
        char* last_start = (char*) &(*last);
        int last_start_size = *last;
        int last_end_size = *((int*) (last_start + (last_start_size < 0 ? -last_start_size : last_start_size) + sizeof(int)));
        return first_start_size == first_end_size && last_start_size && last_end_size;
    }

public:
    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;


    /**
     * O(1) in space, time
     * throw a std::bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    My_Allocator() {
        (*this)[0] = N - 8;
        (*this)[N - 4] = N - 8;
        if (N < (sizeof(T) + (2 * sizeof(int))))
            throw std::bad_alloc();
        assert(valid());
    }

    My_Allocator(const My_Allocator &) = default;
    ~My_Allocator() = default;
    My_Allocator &operator=(const My_Allocator &) = default;

    /** O(1) in space, time */
    void construct(T * p, const T& v) {
        new (p) T(v);
        assert(valid());
    }

    /**
     * O(1) in space, time
     * Allocated memory for s objects of type T.
     * The smallest allowable free block is sizeof(T) + (2 * sizeof(int)).
     * The first free block that is large enough to fit the requested
     * amount of data is used.
     * Throws a std::bad_alloc exception if there isn't an acceptable free block
     */
    T * allocate(std::size_t s) {
        iterator itr = begin();
        std::size_t bytes_seen = 0;
        int bytes_needed = s * sizeof(T);
        while(bytes_seen < N) {
            int size_in_bytes = *itr;
            if(size_in_bytes >= bytes_needed) { //i.e. if it is free block that is big enough
                int bytes_needed_including_sentinels = (bytes_needed + sizeof(T) + (2 * sizeof(int)));
                if(size_in_bytes >= bytes_needed_including_sentinels) {
                    *((int*) (a + bytes_seen)) = -bytes_needed;
                    *((int*) (a + bytes_seen + sizeof(int) + bytes_needed)) = -bytes_needed;
                    int free_block_size = size_in_bytes - bytes_needed - (2 * sizeof(int));
                    *((int*) (a + bytes_seen + sizeof(int) + bytes_needed + sizeof(int))) = free_block_size;
                    *((int*) (a + bytes_seen + size_in_bytes + sizeof(int))) = free_block_size;
                }
                else {
                    *((int*) (a + bytes_seen)) = -size_in_bytes;
                    *((int*) (a + bytes_seen + sizeof(int) + size_in_bytes)) = -size_in_bytes;
                }
                assert(valid());
                return (T*) (a + bytes_seen + 4);
            }
            bytes_seen += (size_in_bytes < 0 ? -size_in_bytes : size_in_bytes) + 2 * sizeof(int); // include space for start/end sentinels
            ++itr;
        }
        throw std::bad_alloc(); //can't allocate this memory - not enough space!
    }

    /**
     * Run the given function on each block (free or busy) of this allocator.
     * The function is passed a single int argument whose absolute value is
     * the size of the block. This number is negative iff the block is busy.
     * */
    template <typename Func>
    void for_each_block(Func func) {
        iterator itr = begin();
        std::size_t bytes_seen = 0;
        while(bytes_seen < N) {
            int size_in_bytes = *itr;
            func(size_in_bytes);
            bytes_seen += (size_in_bytes < 0 ? -size_in_bytes : size_in_bytes) + 2 * sizeof(int); // include space for start/end sentinels
            ++itr;
        }
    }

    /**
     * O(1) in space, time
     * Deallocates the given pointer, which must be a pointer returned
     * from allocate. The memory occupied by the pointer will be
     * available for future allocation after this method returns.
     * After deallocation, adjacent free blocks are coalesced
     * Throws an invalid_argument exception if p is invalid.
     */
    void deallocate(T * p, std::size_t __size_unused) {
        std::size_t byte_index = ((std::size_t) p) - ((std::size_t) a);
        if(byte_index >= N)
            throw std::invalid_argument("invalid pointer, byte index out of bounds");
        iterator itr = iterator(*this, byte_index - sizeof(int));
        int& size = *itr;
        if(size >= 0)
            throw std::invalid_argument("invalid pointer, already freed (size " + std::to_string(size) + ")");
        size *= -1;
        *((int*) (a + byte_index + size)) = size;
        bool hasNext = (byte_index + size + sizeof(int)) < N;
        bool hasPrev = byte_index > sizeof(int);
        if(hasNext) {
            iterator next_itr = itr;
            ++next_itr;
            int next_size = *next_itr;
            if(next_size > 0) {
                int * end_sentinel_ptr = (int*) (a + byte_index + size + (2 * sizeof(int)) + next_size);
                int new_size = size + next_size + (2 * sizeof(int));
                size = new_size;
                *end_sentinel_ptr = new_size;
            }
        }
        if(hasPrev) {
            iterator prev_itr = itr;
            --prev_itr;
            int& prev_size = *prev_itr;
            if(prev_size > 0) {
                int * end_sentinel_ptr = (int*) (a + byte_index + size);
                int new_size = size + prev_size + (2 * sizeof(int));
                prev_size = new_size;
                *end_sentinel_ptr = new_size;
            }
        }
        assert(valid());
    }

    /** O(1) in space, time */
    void destroy(T * p) {
        p->~T();
        assert(valid());
    }

    /** O(1) in space, time */
    int &operator[](int i) {
        return *reinterpret_cast<int *>(&a[i]);
    }

    /** O(1) in space, time */
    const int &operator[](int i) const {
        return *reinterpret_cast<const int *>(&a[i]);
    }

    /** O(1) in space, time */
    iterator begin() {
        return iterator(*this, 0);
    }

    /** O(1) in space, time */
    const_iterator begin() const {
        return const_iterator(*this, 0);
    }

    /** O(1) in space, time */
    iterator end() {
        return iterator(*this, N);
    }

    /** O(1) in space, time */
    const_iterator end() const {
        return const_iterator(*this, N);
    }

};

#endif // Allocator_hpp
