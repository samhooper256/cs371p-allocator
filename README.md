# CS371p: Object-Oriented Programming Allocator Repo

* Name: Sam Hooper

* EID: slh4838

* GitLab ID: samhooper256

* HackerRank ID: samhooper256

* Git SHA: 760fd2646b0661d2258855d9a3584a95296e9029

* GitLab Pipelines: https://gitlab.com/samhooper256/cs371p-allocator/-/pipelines

* Estimated completion time: 6

* Actual completion time: 6

* Comments: None
