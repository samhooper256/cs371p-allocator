// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <string>    // string

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace std;

struct A {
    friend bool operator == (const A&, const A&) {
        A::log += "==(A, A) ";
        return true;
    }

    static string log;

    A  ()         = default;
    A  (int)      {
        log += "A(int) ";
    }
    A  (const A&) {
        log += "A(A) ";
    }
    ~A ()         {
        log += "~A() ";
    }
};

string A::log;

template <typename T, std::size_t N>
void print_allocator(My_Allocator<T, N>& allocator, std::string prefix="") {
    cout << prefix;
    allocator.for_each_block([] (int size) {
        cout << size << ' ';
    });
    cout << endl;
}

TEST(AllocatorFixture, test0) {
    using allocator_type = My_Allocator<A, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type  x;
    const size_type s = 2;
    const pointer   b = x.allocate(s);
    const pointer   e = b + s;

    const value_type v = 0;
    ASSERT_EQ(A::log, "A(int) ");

    pointer p = b;
    while (p != e) {
        x.construct(p, v);
        ++p;
    }
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ");

    ASSERT_EQ(count(b, e, v), ptrdiff_t(s));
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ==(A, A) ==(A, A) ");

    p = e;
    while (b != p) {
        --p;
        x.destroy(p);
    }
    ASSERT_EQ(A::log, "A(int) A(A) A(A) ==(A, A) ==(A, A) ~A() ~A() ");
    x.deallocate(b, s);
}

TEST(AllocatorFixture, test1) {
    My_Allocator<A, 1000> x;        // read/write
    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(AllocatorFixture, test2) {
    const My_Allocator<A, 1000> x;  // read-only
    ASSERT_EQ(x[  0], 992);
    ASSERT_EQ(x[996], 992);
}

TEST(AllocatorFixture, mytest1) {
    using allocator_type = My_Allocator<A, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type  x;
    const size_type s = 2;
    const pointer   b = x.allocate(s);
    x.deallocate(b, s);
}

TEST(AllocatorFixture, mytest2) {
    const My_Allocator<double, 16> x;
    ASSERT_EQ(x[  0], 8);
    ASSERT_EQ(x[12], 8);
}

TEST(AllocatorFixture, mytest3) {
    My_Allocator<double, 16> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
}

TEST(AllocatorFixture, mytest4) {
    My_Allocator<double, 20> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -12);
    ASSERT_EQ(x[16], -12);
}

TEST(AllocatorFixture, mytest5) {
    My_Allocator<double, 20> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -12);
    ASSERT_EQ(x[16], -12);
}

TEST(AllocatorFixture, mytest6) {
    My_Allocator<double, 24> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -16);
    ASSERT_EQ(x[20], -16);
}

TEST(AllocatorFixture, mytest7) {
    My_Allocator<double, 28> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
}

TEST(AllocatorFixture, mytest8) {
    My_Allocator<double, 32> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
}

TEST(AllocatorFixture, mytest9) {
    My_Allocator<double, 32> x;
    x.allocate(1);
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
}

TEST(AllocatorFixture, mytest10) {
    My_Allocator<double, 32> x;
    x.allocate(1);
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
}

TEST(AllocatorFixture, mytest11) {
    My_Allocator<double, 32> x;
    x.allocate(3);
    ASSERT_EQ(x[0], -24);
    ASSERT_EQ(x[28], -24);
}

TEST(AllocatorFixture, mytest12) {
    My_Allocator<double, 32> x;
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
    double * p = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    x.deallocate(p, 8);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
}

TEST(AllocatorFixture, mytest13) {
    My_Allocator<double, 32> x;
    double * p = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
    x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    x.deallocate(p, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
}

TEST(AllocatorFixture, mytest14) {
    My_Allocator<double, 32> x;
    double * p = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
    double * q = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    x.deallocate(q, 8);
    x.deallocate(p, 8);
    ASSERT_EQ(x[0], 24);
    ASSERT_EQ(x[28], 24);
}

TEST(AllocatorFixture, mytest15) {
    My_Allocator<double, 32> x;
    double * p = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], 8);
    ASSERT_EQ(x[28], 8);
    double * q = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    x.deallocate(p, 8); //swapped p and q here
    x.deallocate(q, 8);
    ASSERT_EQ(x[0], 24);
    ASSERT_EQ(x[28], 24);
}

TEST(AllocatorFixture, mytest16) {
    My_Allocator<double, 48> x;
    double * a = x.allocate(1);
    double * b = x.allocate(1);
    double * c = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
}

TEST(AllocatorFixture, mytest17) {
    My_Allocator<double, 48> x;
    double * a = x.allocate(1);
    double * b = x.allocate(1);
    double * c = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
    x.deallocate(a, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
}

TEST(AllocatorFixture, mytest18) {
    My_Allocator<double, 48> x;
    double * a = x.allocate(1);
    double * b = x.allocate(1);
    double * c = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
    x.deallocate(a, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
    x.deallocate(c, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], 8);
    ASSERT_EQ(x[44], 8);
}

TEST(AllocatorFixture, mytest19) {
    My_Allocator<double, 48> x;
    double * a = x.allocate(1);
    double * b = x.allocate(1);
    double * c = x.allocate(1);
    ASSERT_EQ(x[0], -8);
    ASSERT_EQ(x[12], -8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
    x.deallocate(a, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], -8);
    ASSERT_EQ(x[44], -8);
    x.deallocate(c, 8);
    ASSERT_EQ(x[0], 8);
    ASSERT_EQ(x[12], 8);
    ASSERT_EQ(x[16], -8);
    ASSERT_EQ(x[28], -8);
    ASSERT_EQ(x[32], 8);
    ASSERT_EQ(x[44], 8);
    x.deallocate(b, 8);
    ASSERT_EQ(x[0], 40);
    ASSERT_EQ(x[44], 40);
}